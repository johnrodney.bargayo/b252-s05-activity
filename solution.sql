-- 1.

SELECT customerName FROM Customers WHERE Country='Philippines';

-- 2.

SELECT contactLastName, contactFirstName FROM Customers WHERE customerName='La Rochelle Gifts';

-- 3.

SELECT productName, MSRP FROM Products WHERE productName='The Titanic';

-- 4.

SELECT firstName, lastName FROM Employees WHERE email ='jfirrelli@classicmodelcars.com';

-- 5.

SELECT customerName FROM Customers WHERE state IS NULL;

-- 6.

SELECT firstName, lastName, email FROM Employees WHERE lastName = 'Patterson' AND firstName = 'Steve';


-- 7.

SELECT customerName, Country, creditLimit FROM Customers WHERE Country != 'USA' AND creditLimit > 3000;

-- 8.




SELECT customerNumber FROM Orders WHERE comments LIKE '%DHL%';
-- 9.


SELECT productLine FROM Productlines WHERE textDescription LIKE '%state of the art%';
-- 10.


SELECT DISTINCT Country FROM Customers;

-- 11.


SELECT DISTINCT status FROM Orders;

-- 12.


SELECT customerName, Country FROM Customers WHERE country IN ('USA', 'France', 'Canada');

-- 13.



SELECT employees.firstName, employees.lastName, offices.city FROM Employees JOIN offices ON employees.office_code = offices.officeCode WHERE offices.city = 'Tokyo';

-- 14.


SELECT Customers.CustomerName, Employees.employeeNumber FROM Customers INNER JOIN Employees ON Employees.employeeNumber = Customers.salesRepEmployeeNumber WHERE Employees.firstName = 'Leslie' and Employees.lastName = 'Thompson';

-- 15.

SELECT Products.productName, Customers.customerName FROM Orders INNER JOIN Customers ON Orders.customerNumber = Customers.customerNumber INNER JOIN OrderDetails ON Orders.orderNumber = OrderDetails.orderNumber INNER JOIN Products ON OrderDetails.productCode = Products.productCode WHERE Customers.customerName = 'Baane Mini Imports';

-- 16.



SELECT employees.firstName, employees.lastName, customers.CustomerName, offices.country FROM employees
	JOIN customers ON employees.employeeNumber = customers.salesRepEmployeeNumber
	JOIN offices ON employees.officeCode = offices.officeCode WHERE customers.country = offices.country;


-- 17.

SELECT productName, quantityInStock FROM products WHERE productLine = 'planes' AND quantityInStock < 1000;


-- 18.

SELECT customerName FROM customers WHERE phone LIKE '%+81%';
